class Node{
    constructor(element){
        this.element = element;
        this.next = null;
    }
}

class LinkedList{
    constructor(){
        this.head = null;
        this.size = 0;
    }

    add(element){
        var node = new Node(element);
        var current;

        if(this.head == null){
            this.head = node;
        }
        else{
            current = this.head;
            while(current.next){
                current = current.next;
            }
            current.next = node;
        }
        this.size++;
        this.printList();
    }

    remove(element){
        var current = this.head;
        var prev = null;
        while (current != null) {
            if (current.element === element){
                if (prev == null) {
                    this.head = current.next;
                } else {
                    prev.next = current.next;
                }
                this.size--;
                this.printList();
                return current.element;
            }
            prev = current;
            current = current.next;
        }
        this.printList();
        return -1;
    }

    printList()
    {
        var str = "";
        var curr = this.head;
        while (curr) {
            str += curr.element + " ";
            curr = curr.next;
        }
        console.log(str);
        document.getElementById('data1').innerHTML = str;
    }
    
}

var list = new LinkedList();
console.log(list);